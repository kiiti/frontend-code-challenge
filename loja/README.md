# Loja Exemplo em React

## Tecnologia utilizada
 - React
 - Node
 - Gulp
 - Sass

## Dependências
- Node.js
- Gulp

## Instalação
1. Instale o pacote:
``$ npm install``

## Estilos
Caso queira alterar qualquer arquivo de estilo:

1. ative o Gulp no terminal
``$ gulp``

2. altere o arquivo desejado na pasta **scr/Assets/scss**
3. Ao salvar, automaticamente um arquivo minificado será gerado na pasta **scr/Assets/css**