import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from "prop-types";

class Header extends Component {
    constructor(props) {
        super(props);
        const menu = [
            {
                name: 'Todas as categorias',
                url: '/',
            },
            {
                name: 'Eletrônicos',
                url: '/',
            },
            {
                name: 'Smartphones',
                url: '/',
            },
            {
                name: 'Games',
                url: '/',
            },
            {
                name: 'Livros',
                url: '/',
            },
        ];
    }
    render() {
        console.log(this.props);
        return (
            <header>
                <a className="logo">
                    webstore
                </a>
                <nav className="menu">
                    <div className="container">
                        <ul>
                            <li>
                                <NavLink
                                    exact to="/"
                                    className="menu__item"
                                    activeClassName="menu__item--active"
                                >
                                    Todas as categorias
                                </NavLink>
                            </li>
                            <li>
                                <NavLink
                                    to="/Company"
                                    className="menu__item"
                                    activeClassName="menu__item--active"
                                >
                                    Eletrônicos
                                </NavLink>
                            </li>
                            <li><a href="" className="menu__item" activeClassName="menu__item--active">Smartphones</a></li>
                            <li><a href="" className="menu__item" activeClassName="menu__item--active">Games</a></li>
                            <li><a href="" className="menu__item" activeClassName="menu__item--active">Livros</a></li>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}

Header.propTypes = {
    menu: PropTypes.array,
};

Header.defaultProps = {
    menu: [],
};

export default Header;
