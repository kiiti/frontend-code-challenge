import React, { Component } from 'react';

class Company extends Component {
    render() {
        return (
            <div className="wrap">
                <h1>Empresa</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel convallis urna. Morbi sagittis leo nulla, et euismod diam accumsan ac. Aliquam erat volutpat. Cras sed quam id ex faucibus laoreet a vitae erat. Vestibulum et pretium dolor, vitae congue ligula. Quisque scelerisque lobortis purus. Nulla vel arcu orci. Maecenas tristique gravida velit non vestibulum. Aliquam varius efficitur convallis. Etiam elementum ante ligula, semper blandit tortor consequat vel. Morbi non dictum ipsum, eget sagittis massa. Aenean et hendrerit tortor, eget posuere sem. Integer aliquam porttitor augue et accumsan. Duis tempus suscipit lorem, vel venenatis enim dapibus ac. Cras vitae turpis a lorem porttitor vulputate sed in dolor.</p>

                <p>Fusce posuere, augue at varius dignissim, enim augue aliquam leo, in accumsan mauris mauris accumsan elit. Proin lacinia varius ligula et volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer aliquet blandit consequat. Donec blandit est sit amet vulputate ornare. Donec mauris sapien, venenatis ac feugiat a, rhoncus ut sapien. In hac habitasse platea dictumst. Vivamus hendrerit eleifend urna ut rutrum. Suspendisse rutrum ac metus in consectetur. Nunc eu lectus eu felis dictum maximus. Donec luctus vestibulum tellus, ut elementum dolor fringilla a. Fusce neque nulla, aliquam et mattis quis, facilisis vel augue. Quisque sed sapien ac mi lobortis tempus. Mauris interdum nibh ac lectus tincidunt luctus.</p>

                <p>Maecenas cursus scelerisque mauris a consectetur. Nam vulputate vitae nisi quis placerat. Nunc lacinia bibendum eros, ut sollicitudin mauris lacinia vitae. Integer velit leo, consequat quis varius id, eleifend sed dolor. Integer lacinia sit amet purus at fermentum. Nunc feugiat accumsan pharetra. Donec in pulvinar elit. Praesent hendrerit diam quis libero porttitor fringilla. Phasellus felis massa, dictum at justo eu, tristique porttitor mi. Aenean auctor odio a sodales bibendum. Morbi pharetra quam vel massa congue, sit amet tristique felis condimentum. Sed condimentum nec lectus nec rutrum. Phasellus faucibus lectus at purus posuere aliquet. Nam iaculis ac lectus ac vulputate.</p>

                <p>Donec mollis vulputate tortor et scelerisque. Sed ornare ex vitae tempus tempus. Quisque ut quam dolor. Nam iaculis nibh at nisl sagittis, sit amet blandit ex fermentum. Integer luctus ullamcorper nunc, at accumsan velit finibus quis. Duis feugiat viverra dignissim. Ut semper ante eu iaculis condimentum.</p>

                <p>Donec eleifend lobortis mauris, eu consequat felis viverra a. Donec id tristique ante. Mauris porttitor nisl nec leo efficitur, a congue massa maximus. Etiam placerat justo tellus, nec dictum odio facilisis vel. Aliquam erat volutpat. Vestibulum ultrices, mauris ut scelerisque iaculis, libero dolor scelerisque ex, at cursus sem leo nec orci. Maecenas viverra quam turpis, et varius nisi varius nec. Nullam vestibulum ultrices turpis, at rhoncus purus ornare sit amet.</p>
            </div>
        );
    }
}

export default Company;
