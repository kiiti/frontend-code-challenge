import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <nav>
                        <h3>A Empresa</h3>
                        <a href="#">Quem somos</a>
                        <a href="#">Localização</a>
                        <a href="#">Contato</a>
                    </nav>
                    <nav>
                        <h3>Categorias</h3>
                        <a href="#">Eletrônicos</a>
                        <a href="#">Smartphones</a>
                        <a href="#">Games</a>
                        <a href="#">Livros</a>
                    </nav>
                    <nav>
                        <h3>Minha Conta</h3>
                        <a href="#">Entrar na Conta</a>
                        <a href="#">Cadastrar</a>
                        <a href="#">Informações de Conta</a>
                        <a href="#">Meus Pedidos</a>
                        <a href="#">Endereços</a>
                    </nav>
                    <nav>
                        <h3>Suporte</h3>
                        <a href="#">Política de Privacidade</a>
                        <a href="#">Troca e Devolução</a>
                        <a href="#">Política de Entrega</a>
                        <a href="#">Política de Pagamento</a>
                    </nav>
                    <nav>
                        <h3>Atendimento</h3>
                        <h4>Centra de Atendimento</h4>
                        <p>0800 222 0000</p>

                        <a href="#">Chat</a>
                        <a href="#">Fale Conosco</a>
                    </nav>
                    <nav>
                        <h3>Social</h3>
                        <a href="#">Facebook</a>
                        <a href="#">Instagram</a>
                        <a href="#">Youtube</a>
                    </nav>
                </div>
            </footer>        
        );
    }
}

export default Footer;
